# API REST interna Node - No se debe exponer el puerto

API Rest escucha por defecto en el puerto 4000

## Instalar dependencias
```
npm install
```

## Ejecutar proyecto
```
node src/index.js
```

## Servicio REST expuesto
```
Servicio GET: http://localhost:4000
```

## Generar imagen docker
```
docker build -t api-interna-node .
```
