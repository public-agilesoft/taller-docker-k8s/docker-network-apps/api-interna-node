FROM node:12-alpine

WORKDIR /usr/src/app

COPY . .

RUN npm install --production

ENV PORT=4000

EXPOSE ${PORT}

CMD ["node", "src/index.js"]
