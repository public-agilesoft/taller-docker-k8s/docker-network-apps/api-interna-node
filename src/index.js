'use strict';

const express = require('express');
const app = express();
const port = process.env.PORT | 4000;

app.get('/', (req, res) => {
    const date = new Date();
    console.log(`Internal API - Request received at ${date}`);
    res.json({
        data: `Hi! I am an internal Rest service ${date}`,
        id: `${process.pid}`,
    });
});

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`);
});
